import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String english = "Hello Flutter";
String japan = "Flutter-kun konnichiwa!";
String thai = "สวัสดี Flutter";
String korea = "Annyeong Flutter";
class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = english;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: IconButton(
              onPressed: () {},
              icon: Icon(Icons.home),
            ),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == english ? thai : english;
                    });
                  },
                  icon: Icon(Icons.translate)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == english ? japan : english;
                    });
                  },
                  icon: Icon(Icons.g_translate)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == english ? korea : english;
                    });
                  },
                  icon: Icon(Icons.language))
            ],
          ),
          body: Center(
            child: Text(displayText, style: const TextStyle(fontSize: 25)),
          ),
        ));
  }
}

/*class HelloFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: IconButton(onPressed: (){}, icon: Icon(Icons.home),),
            actions: <Widget>[
              IconButton(onPressed: () {}, icon: Icon(Icons.translate))
            ],
          ),
          body: Center(
            child: Text(
                "Hello Flutter!",
                style: TextStyle(fontSize: 25)

            ),
          ),
        ));
  }*/

